// Documentação: https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Array

// FIFO: First In, First Out (o primeiro valor inserido será também o primeiro a ser removido)

class Fila {

  constructor() {
    this.lista = new Array();
  }

  enqueue(e) {
    // Adiciona um elemento na fila
    this.lista.push(e);
  }

  dequeue() {
    // Retorna o elemento do começo da fila, removendo ele.
    if (this.isEmpty()) {
      return null;
    }
    return this.lista.shift();
  }

  size() {
    // Retorna a quantidade de elementos na fila
    return this.lista.length;
  }

  isEmpty() {
    // Retorna um boleano indicando se a fila está vazia ou não
    return this.lista.length == 0;
  }

  front() {
    // Retorna o elemento do começo da fila, mas não remove.
    if (this.isEmpty()) {
      return null;
    }
    return this.lista[0];
  }
}

module.exports = Fila;