// Documentação: https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Array

// LIFO: Last In, First Out (o último valor inserido será o primeiro a ser removido)

class Pilha {

  constructor() {
    this.lista = new Array();
  }

  push(e) {
    // Adiciona um elemento no topo da pilha
    this.lista.unshift(e);
  }

  pop() {
    // Retorna o elemento do topo da pilha, removendo ele.
    if(this.isEmpty()) {
      return null;
    }
    return this.lista.shift();
  }

  size() {
    // Retorna a quantidade de elementos na pilha
    return this.lista.length;
  }

  isEmpty() {
    // Retorna um boleano indicando se a pilha está vazia ou não
    return this.size() == 0;
  }

  top() {
    // Retorna o elemento do topo da pilha, mas não remove.
    if(this.isEmpty()) {
      return null;
    }
    return this.lista[0];
  }

}

module.exports = Pilha;