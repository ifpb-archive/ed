// O BubbleSort (método da bolha) é um algoritmo de ordenação que compara itens vizinhos de um vetor.
// Se os dois itens comparados na iteração não estiverem em ordem (do maior para o menor, por exemplo), suas posições são trocadas.
// A execução no melhor caso é O(n), e no pior caso, O(n^2)

function bubbleSort(items) { // recebe como parâmetro um vetor
    let swap;
    let last = items.length - 1; // último item do vetor
    do {
        swap = false; // ainda não foi feita a troca
            for(let i = 0; i < last; i++) { // itera por todos os itens desordenados
                if(items[i] > items[i + 1]) { // compara o valor, de dois em dois
                    [items[i], items[i + 1]] = [items [i + 1], items[i]]; // troca de posição usando o destructuring
                    swap = true; // foi feita uma troca
                }
            }
            last--; // após a iteração, o último item já estará em ordem. na próxima, então, não precisamos contar com ele
    } while(swap); // repetir o processo até não ter sido realizada mais nenhuma troca
    return items;
}

console.log(bubbleSort([13, 5, 2, 1, 3, 8]));