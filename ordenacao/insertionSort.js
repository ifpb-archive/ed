// O Insertion Sort é um algoritmo que itera por cada um dos elementos de um vetor e compara seu valor com os elementos anteriores à ele.
// Caso o elemento comparado seja maior, ele anda uma casa no vetor.
// Isso é feito até não ter nenhum elemento maior que o atual antes dele.

function insertionSort(vetor) {
    let atual;
    for(let i = 1; i < vetor.length; i++) { // a comparação começa com o segundo elemento, já que o primeiro não tem nenhum antes dele
        let j = i - 1;
        atual = vetor[i];
        while(j >= 0 && atual < vetor[j]) { // compara se o elemento anterior é maior que o atual
            vetor[j + 1] = vetor[j]; // se for maior, atribui o valor do próximo elemento a ele
            j--; // na próxima iteração, compara com o elemento anterior ao anterior
        }
        vetor[j + 1] = atual; // quando só existirem elementos menores que o atual antes dele, podemos inserí-lo depois deles
    }
    return vetor;
}

console.log(insertionSort([13, 5, 2, 1, 3, 8]));