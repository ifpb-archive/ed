// O SelectionSort é um algoritmo que faz iterações em busca dos menores elementos de um vetor.
// A ordenação é feita levando esses menores elementos para as primeiras posições do vetor.
// A execução no melhor e pior caso é O(n^2)

function selectionSort(vetor) {
    let menor;
    for(let i = 0; i < vetor.length - 1; i++) {
        menor = i; // considera inicialmente que o primeiro elemento é o menor
        for(let j = i + 1; j < vetor.length; j++) { // itera por todos os elementos depois desse
            if(vetor[j] < vetor[menor]) { // compara se são menores que ele
                menor = j;
            }
        } if(menor != i) {
            [vetor[i], vetor[menor]] = [vetor[menor], vetor[i]]; // se algum for, realiza a troca levando-o para a primeira posição
        }
    }
    return vetor;
}

console.log(selectionSort([13, 5, 2, 1, 3, 8]));