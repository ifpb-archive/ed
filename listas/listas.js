class Nó {

    constructor(valor, prox) {
        this.valor = valor;
        this.prox = prox;
    }

}

class ListaSimplesmenteEncadeada {

    constructor() {
        this.cabeça = null;
    }

    adicionar(valor) {
        // 1° passo - Criar o nó que armazena o novo valor a ser adicionado
        let novoNó = new Nó(valor, null);

        // 2° passo - Adicionar o novo nó no final da lista
        if (this.cabeça == null) {
            // Caso mais simples, minha lista está vazia ainda
            this.cabeça = novoNó;
        } else {
            // Buscar o último nó da minha lista (aquele que aponta para vazio, ou seja, aponta para null)
            let nóAtual = this.cabeça;
            while(nóAtual.prox != null) {
                nóAtual = nóAtual.prox;
            }
            nóAtual.prox = novoNó;
        }
    }

    imprimir() {
        let nóAtual = this.cabeça;
        while(nóAtual != null) {
            console.log(nóAtual.valor);
            nóAtual = nóAtual.prox;
        }
    }

    toArray() {
        let array = new Array();

        let nóAtual = this.cabeça;
        while(nóAtual != null) {
            //console.log(nóAtual.valor);
            array.push(nóAtual.valor);
            nóAtual = nóAtual.prox;
        }

        return array;
    }

    tamanho() {
        let cont = 0;

        let nóAtual = this.cabeça;
        while(nóAtual != null) {
            //console.log(nóAtual.valor);
            //array.push(nóAtual.valor);
            cont += 1;
            nóAtual = nóAtual.prox;
        }

        return cont;
    }

    obter(índice) {
        if (índice < 0) {
            return null;
        }
        if (índice >= this.tamanho()) {
            return null;
        }

        let nóAtual = this.cabeça;
        let índiceAtual = 0;
        while(nóAtual != null) {
            if (índiceAtual == índice) {
                break;
            }
            nóAtual = nóAtual.prox;
            índiceAtual++;
        }

        return nóAtual.valor;
    }

    remover(elemento) {

        if (this.cabeça == null) {
            return;
        }

        let nóAtual = this.cabeça;
        let nóAnterior = null;
        while(nóAtual != null) {
            if (nóAtual.valor == elemento) {
                break;
            }
            nóAnterior = nóAtual;
            nóAtual = nóAtual.prox;
        }

        if (nóAtual == null) {
            // 1. nóAtual == null => não encontrei o nó que possui o elemento a ser removido => nada a fazer
            return;
        }

        // 2. nóAtual vai apontar para o nó que possui o elemento a ser removido
        if (nóAtual == this.cabeça) {
            // 2.1 nóAtual é o primeiro da lista
            this.cabeça = nóAtual.prox;
        } else {
            // 2.2 nóAtual NÃO é o primeiro da lista
            nóAnterior.prox = nóAtual.prox;
        }
    }

}

/*
let lista = new ListaSimplesmenteEncadeada();
lista.adicionar("A");
lista.adicionar("C");
lista.adicionar("B");
lista.adicionar("D");
console.log(lista.toArray());
*/


module.exports = ListaSimplesmenteEncadeada;