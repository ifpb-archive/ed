const ListaSimplesmenteEncadeada = require('./listas');

test('Adicionar um valor', () => {
    let lista = new ListaSimplesmenteEncadeada();
    lista.adicionar(1);
    expect(lista.toArray()).toStrictEqual([1]);
});

test('Adicionar múltiplos valores', () => {
    let lista = new ListaSimplesmenteEncadeada();
    lista.adicionar(1);
    lista.adicionar(2);
    lista.adicionar(3);
    expect(lista.toArray()).toStrictEqual([1, 2, 3]);
});

test('Contagem - Lista vazia', () => {
    let lista = new ListaSimplesmenteEncadeada();
    expect(lista.tamanho()).toBe(0);
});

test('Contagem - Lista com um elemento', () => {
    let lista = new ListaSimplesmenteEncadeada();
    lista.adicionar(1);
    expect(lista.tamanho()).toBe(1);
});

test('Contagem - Lista com 5 elementos', () => {
    let lista = new ListaSimplesmenteEncadeada();
    lista.adicionar(1);
    lista.adicionar(2);
    lista.adicionar(3);
    lista.adicionar(4);
    lista.adicionar(5);
    expect(lista.tamanho()).toBe(5);
});

test('Recuperação - Índice inválido - Índice negativo', () => {
    let lista = new ListaSimplesmenteEncadeada();
    expect(lista.obter(-1)).toBe(null);
});

test('Recuperação - Índice inválido - Índice maior que quantidade de elementos', () => {
    let lista = new ListaSimplesmenteEncadeada();
    lista.adicionar(1);
    lista.adicionar(2);
    lista.adicionar(3); // índice >= tamanho => problema, índice válido
    expect(lista.obter(3)).toBe(null);
});

test('Recuperação - Índice Válido - 0', () => {
    let lista = new ListaSimplesmenteEncadeada();
    lista.adicionar(1);
    lista.adicionar(2);
    lista.adicionar(3);
    expect(lista.obter(0)).toBe(1);
});

test('Recuperação - Índice Válido - 1', () => {
    let lista = new ListaSimplesmenteEncadeada();
    lista.adicionar(1);
    lista.adicionar(2);
    lista.adicionar(3);
    expect(lista.obter(1)).toBe(2);
});

test('Recuperação - Índice Válido - 2', () => {
    let lista = new ListaSimplesmenteEncadeada();
    lista.adicionar(1);
    lista.adicionar(2);
    lista.adicionar(3);
    expect(lista.obter(2)).toBe(3);
});

test('Remoção - Lista vazia', () => {
    let lista = new ListaSimplesmenteEncadeada();
    lista.remover(2);
    expect(lista.toArray()).toStrictEqual([]);
});

test('Remoção - Lista NÃO vazia - Elemento não encontrado', () => {
    let lista = new ListaSimplesmenteEncadeada();
    lista.adicionar(1);
    lista.adicionar(2);
    lista.adicionar(3);
    lista.remover(5);
    expect(lista.toArray()).toStrictEqual([1, 2, 3]);
});

test('Remoção - Lista NÃO vazia - Elemento a ser removido está no índice 0', () => {
    let lista = new ListaSimplesmenteEncadeada();
    lista.adicionar(1);
    lista.adicionar(2);
    lista.adicionar(3);
    lista.adicionar(1);
    lista.remover(1);
    expect(lista.toArray()).toStrictEqual([2, 3, 1]);
});

test('Remoção - Lista NÃO vazia - Elemento a ser removido está no índice 1', () => {
    let lista = new ListaSimplesmenteEncadeada();
    lista.adicionar(1);
    lista.adicionar(2);
    lista.adicionar(3);
    lista.remover(2);
    expect(lista.toArray()).toStrictEqual([1, 3]);
});

test('Remoção - Lista NÃO vazia - Elemento a ser removido está no índice 2', () => {
    let lista = new ListaSimplesmenteEncadeada();
    lista.adicionar(1);
    lista.adicionar(2);
    lista.adicionar(3);
    lista.remover(3);
    expect(lista.toArray()).toStrictEqual([1, 2]);
});
