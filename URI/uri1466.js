const ÁrvoreBinária = require('../arvores/ArvoreBinaria');

var input = require('fs').readFileSync('stdin', 'utf8');
var [c, ...rest] = input.split('\n');

for(let i = 1; i <= c; i++) {
    let qnt = rest.shift();
    let valores = rest.shift().split(' ');
    let arvore = new ÁrvoreBinária();
    for(let j = 0; j < qnt; j++) {
        let valor = valores.shift().trim();
        arvore.inserir(Number(valor));
    }
    let percurso = arvore.percursoPorNível();
    console.log(`Case ${i}:`);
    console.log(percurso.join(' '));
    console.log();
}