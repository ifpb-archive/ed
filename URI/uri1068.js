const Pilha = require("../filas-pilhas/pilhas");

let input = require('fs').readFileSync('stdin', 'utf8');
let lines = input.split('\n');

for(line of lines){
    let pilha = new Pilha();
    let temParentese = false;
    for(l of line){
        if(l == '('){
            pilha.push(l);
            temParentese = true;
        } else if(l == ')'){
            if(pilha.top() == '('){
                pilha.pop();
            } else {
                pilha.push(l);
            }
        }
    }
    if(temParentese) {
        pilha.isEmpty()? console.log('correct') : console.log('incorrect');
    }
}