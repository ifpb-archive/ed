// Em cada posição da lista, armazene uma string que represente a nota correspondente do aluno
// caso ele obtenha aquela nota do respectivo índice!

var input = require('fs').readFileSync('stdin', 'utf8');
var [n] = input.split('\n');

let notas = [];
notas.push('E');
for(i = 1; i <= 35; i++) {
    notas.push('D');
}
for(i = 36; i <= 60; i++) {
    notas.push('C');
}
for(i = 61; i <= 85; i++) {
    notas.push('B');
}
for(i = 86; i <= 100; i++) {
    notas.push('A');
}
console.log(notas[n]);