// Cada caso de teste deve trabalhar individualmente com o array.
// Ou seja, não armazene todos os valores ao mesmo tempo no array.
// Trabalhe com um array de 3 posições, que será utilizado para guardar os 3 valores de cada caso de teste.

var input = require('fs').readFileSync('stdin', 'utf8');
let [n, ...rest] = input.split('\n');

for(i = 0; i < n; i++) {
    let valores = rest.shift().split(' ');
    let notas = [];
    for(j = 0; j < 3; j++){
        notas.push(valores.shift());
    }
    console.log(((notas[0]*2 + notas[1]*3 + notas[2]*5)/ (2+3+5)).toFixed(1));
}