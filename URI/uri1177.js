// Use um loop de iteração para preencher os valores na estrutura de dados,
// e só então um outro loop de iteração para imprimir os valores.

var input = require('fs').readFileSync('stdin', 'utf8');
var [t] = input.split('\n');

let n = [];
for(i = 0; i < 1000; i++) {
    n.push(i % t);
}
let posicao = 0;
for(valor of n){
    console.log(`N[${posicao}] = ${valor}`);
    posicao++;
}