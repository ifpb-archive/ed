var input = require('fs').readFileSync('stdin', 'utf8');
var lines = input.split('\n');

let valor = parseFloat(lines.shift());
let imposto;
if(valor <= 2000) {
    console.log("Isento");
} else {
    valor-=2000;
    if(valor <= 1000) {
        imposto = 0.08 * valor;
    } else {
        valor-=1000;
        if(valor <= 1500) {
            imposto = 80 + 0.18 * valor;
        } else {
            valor-=1500;
            imposto = 350 + 0.28 * valor;
        }
    }
    console.log(`R$ ${imposto.toFixed(2)}`);
}