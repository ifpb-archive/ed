const ÁrvoreBinária = require('../arvores/ArvoreBinaria');

var input = require('fs').readFileSync('stdin', 'utf8');
var [c, ...rest] = input.split('\n');

for(let i = 1; i <= c; i++) {
    let qnt = rest.shift();
    let valores = rest.shift().split(' ');
    let arvore = new ÁrvoreBinária();
    for(let j = 0; j < qnt; j++) {
        let valor = valores.shift().trim();
        arvore.inserir(parseFloat(valor));
    }
    let pre = arvore.percursoPréOrdem();
    let em = arvore.percursoEmOrdem();
    let pos = arvore.percursoPósOrdem();
    console.log(`Case ${i}:`);
    console.log(`Pre.: ${pre.join(' ')}`);
    console.log(`In..: ${em.join(' ')}`);
    console.log(`Post.: ${pos.join(' ')}`);
    console.log();
}