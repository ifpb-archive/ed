var input = require('fs').readFileSync('stdin', 'utf8');
var [hora_ini, min_ini, hora_fin, min_fin] = input.split(' ').map(e => parseInt(e));

dif = ((hora_fin * 60) + min_fin) - ((hora_ini * 60) + min_ini);
if(dif<=0) {
  dif+=24*60;
}  
let horas = parseInt(dif/60);
let minutos =dif%60;
console.log(`O JOGO DUROU ${horas} HORA(S) E ${minutos} MINUTO(S)`);