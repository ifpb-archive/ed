var input = require('fs').readFileSync('stdin', 'utf8');
var lines = input.split('\n');

let menor = parseInt(lines.shift());
let maior = parseInt(lines.shift());
if(menor > maior) {
    [menor, maior] = [maior, menor];
}
let soma = 0;
for(let i = menor; i <= maior; i++) {
  if(i%13 != 0) {
    soma+=i;
  }
}
console.log(soma);