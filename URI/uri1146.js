var input = require('fs').readFileSync('stdin', 'utf8');
var lines = input.split('\n');

let x = parseInt(lines.shift());
while(x != 0) {
    let sequencia = [];
    for(let i = 1; i <= x; i++) {
        sequencia.push(i);
    }
    console.log(sequencia.join(' '));
    x = parseInt(lines.shift());
}