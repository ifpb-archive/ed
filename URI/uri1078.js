// Armazene no array o resultado da tabuada de multiplicação para cada valor.
// Depois faça a iteração no array para imprimir cada resultado

var input = require("fs").readFileSync("stdin", "utf8");
let [n] = input.split("\n");

let tabuada = [];
for(i = 0; i < 10; i++) {
    tabuada.push(n*(i+1));
    console.log(`${i+1} x ${n} = ${tabuada[i]}`);
}