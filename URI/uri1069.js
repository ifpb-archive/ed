const Pilha = require("../filas-pilhas/pilhas");

let input = require('fs').readFileSync('stdin', 'utf8');
let [n, ...rest] = input.split('\n');

for(i = 0; i < n; i++) {
    let test = rest.shift();
    let pilha = new Pilha();
    let cont = 0;
    for(t of test){
        if(t == '<'){
            pilha.push(t);
        } else if(t == '>' && pilha.top() == '<'){
            pilha.pop();
            cont++;
        }
    }
    console.log(cont);
}