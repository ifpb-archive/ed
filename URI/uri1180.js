var input = require('fs').readFileSync('stdin', 'utf8');
var lines = input.split('\n');

/* let n = parseInt(lines.shift());
let vetor = lines.shift().split(' ').map(e => parseInt(e));
let menor = vetor[0];
let posicao = 0;
for(let i = 0; i < n; i++) {
    if(vetor[i] <= menor) {
        menor = vetor[i];
        posicao = i;
    }
}
console.log(`Menor valor: ${menor}`);
console.log(`Posicao: ${posicao}`); */

let N = parseInt(lines.shift());
let lista = [N];
let linha2 = lines.shift().split(' ');

for (let i = 0; i < N; i++) {
    lista[i] = parseInt(linha2.shift());
}

let menor = lista[0];
let posMenor = 0;

for(let i = 0; i < lista.length; i++){
  let atual = lista[i];
 
  if(atual < menor){
    menor = atual;
    posMenor = i;
    
  }
}

console.log("Menor valor: " + menor);
console.log("Posicao: " + posMenor);