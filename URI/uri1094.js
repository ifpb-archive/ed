// Armazene no array a quantidade acumulada de cada cobaia.
// Como no problema temos 3 tipos de cobaia, use um array de tamanho 3 para isso.

var input = require('fs').readFileSync('stdin', 'utf8');
var [n, ...rest] = input.split('\n');

let cobaias = [];
for(i = 0; i < 3; i++){
    cobaias.push(0);
}
for(i = 0; i < n; i++){
    let exp = rest.shift().split(' ');
    let qnt = parseInt(exp.shift());
    let tipo = exp.shift().trim();
    if(tipo == 'C'){
        cobaias[0]+=qnt;
    } else if(tipo == 'R'){
        cobaias[1]+=qnt;
    } else if(tipo == 'S'){
        cobaias[2]+=qnt;
    }
}
let total = cobaias[0]+cobaias[1]+cobaias[2];
console.log(`Total: ${total} cobaias`);
console.log(`Total de coelhos: ${cobaias[0]}`);
console.log(`Total de ratos: ${cobaias[1]}`);
console.log(`Total de sapos: ${cobaias[2]}`);
console.log(`Percentual de coelhos: ${((cobaias[0]*100)/total).toFixed(2)} %`);
console.log(`Percentual de ratos: ${((cobaias[1]*100)/total).toFixed(2)} %`);
console.log(`Percentual de sapos: ${((cobaias[2]*100)/total).toFixed(2)} %`);