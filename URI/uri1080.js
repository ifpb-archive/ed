// Não faça uso de ""Math.max"" para obter o valor máximo.
// Não faça uso de ""indexOf"" para identificar a posição do maior elemento no array.
// A ideia é vocês praticarem o uso da estrutura de dados array e algoritmos.
// Armazene todos os valores da entrada na lista, para só depois buscar pelo MAIOR e seu respectivo índice!

var input = require('fs').readFileSync('stdin', 'utf8');
var lines = input.split('\n');

let valores = [];
let [maior, cont, posicao] = [0, 1, 1];
for(i = 0; i < 100; i++) {
    valores.push(parseInt(lines.shift()));
}
for(valor of valores){
    if(valor > maior) {
        [maior = valor, posicao = cont];
    }
    cont++;
}
console.log(maior);
console.log(posicao);