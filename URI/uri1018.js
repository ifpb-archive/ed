var input = require('fs').readFileSync('./stdin', 'utf8');
var lines = input.split('\n');

let n = parseInt(lines.shift());
let resto = n;
console.log(n);
let cedulas = [100, 50, 20, 10, 5, 2, 1];
let qnt;
for(cedula of cedulas){
    if(resto >= cedula){
        qnt = resto/cedula;
        resto = resto%cedula;
    } else {
        qnt = 0;
    }
    console.log(`${parseInt(qnt)} nota(s) de R$ ${cedula},00`)
}