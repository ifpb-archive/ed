var input = require('fs').readFileSync('stdin', 'utf8');
var [n] = input.split('\n');

let valores = [];
for(i = 1; i <= n; i++){
    valores.push(i ** 2);
    if(i%2 === 0){
        console.log(`${i}^2 = ${valores[i-1]}`);
    }
}