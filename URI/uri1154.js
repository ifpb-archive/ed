var input = require('fs').readFileSync('stdin', 'utf8');
var lines = input.split('\n');

let idades = [];
idades.push(parseInt(lines.shift()));
let [soma, qnt] = [0, 0];
while(idades[qnt] >= 0) {
    soma+=idades[qnt];
    qnt++;
    idades.push(parseInt(lines.shift()));
}
console.log((soma/qnt).toFixed(2));