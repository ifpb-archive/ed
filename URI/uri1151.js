// Armazene no array os resultados parciais da sequência de Fibonnaci até o valor de índice N.
// Em seguida, imprima os valores no formato solicitado no problema.

var input = require('fs').readFileSync('stdin', 'utf8');
var [n] = input.split('\n');

let fibonacci = [];
for(i = 0; i < n; i++){
    if(i === 0 || i == 1){
        fibonacci.push(i);
    } else{
        fibonacci.push(fibonacci[i-2] + fibonacci[i-1]);
    }
}
console.log(fibonacci.join(' '));