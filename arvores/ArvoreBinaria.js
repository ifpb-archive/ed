// As operações de percurso foram adaptadas conforme a questão 1195 do URI.

const Nó = require("./No");
const Fila = require("../filas-pilhas/filas");

class ÁrvoreBinária {

    constructor() {
        this.raiz = null;
    }
  
    inserir(valor) {

        let novoNó = new Nó(valor);

        if(this.raiz == null) {
            // Árvore vazia
            this.raiz = novoNó;
        } else {
            // Árvore NÃO vazia
            this.raiz.inserir(novoNó);
        }
  
    }

    busca(valorBuscado) {

        if (this.raiz == null) {
            return false;
        } else {
            return this.raiz.busca(valorBuscado);
        }
    
    }

    percursoPréOrdem() {

        let lista = [];
        // Cria uma lista que vai aumentando a medida que os valores dos nós são adicionados
        if(this.raiz) {
            // Árvore NÃO vazia
            this.raiz.percursoPréOrdem(lista);
        }
        return lista;

    }

    percursoEmOrdem() {

        let lista = [];
        // Cria uma lista que vai aumentando a medida que os valores dos nós são adicionados
        if(this.raiz) {
            // Árvore NÃO vazia
            this.raiz.percursoEmOrdem(lista);
        }
        return lista;

    }

    percursoPósOrdem() {

        let lista = [];
        // Cria uma lista que vai aumentando a medida que os valores dos nós são adicionados
        if(this.raiz) {
            // Árvore NÃO vazia
            this.raiz.percursoPósOrdem(lista);
        }
        return lista;

    }

    percursoPorNível() {

        let noAtual = this.raiz;
        let fila = new Fila();
        fila.enqueue(noAtual);
        let lista = [];
        while(!fila.isEmpty()) {
            noAtual = fila.dequeue();
            lista.push(noAtual.valor);
            if(noAtual.esquerda) {
                fila.enqueue(noAtual.esquerda);
            }
            if(noAtual.direita) {
                fila.enqueue(noAtual.direita);
            }
        }
        return lista;
    }

}

module.exports = ÁrvoreBinária;