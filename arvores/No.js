// As operações de percurso foram adaptadas conforme a questão 1195 e 1466 do URI.

class Nó {

  constructor(valor) {
    this.valor = valor;
    this.esquerda = null;
    this.direita = null;
  }

  inserir(novoNó) {
   
    if (this.valor > novoNó.valor) {
      // Valor sendo inserido é MENOR que o valor do nó em mãos.
      // Tenho que adicionar na subárvore da ESQUERDA.
      if(this.esquerda == null) {
        // Não tenho subárvore à esquerda.
        this.esquerda = novoNó;
      } else {
        // Existe SIM subárvore à esquerda.
        // O nó dessa subárvore então passará a ser o nó "raiz" analisado
        this.esquerda.inserir(novoNó); // Recursividade (uma função chama a ela mesma).
      }
  
    } else if (this.valor < novoNó.valor) {
      // Valor sendo inseiro é MAIOR que o valor do nó em mãos.
      // Tenho que adicionar na subárvore da DIREITA.
      if (this.direita == null) {
        // Não existe subárvore à direita.
        this.direita = novoNó;
      } else {
        // Existe SIM subárvore à direita.
        // O nó dessa subárvore então passará a ser o nó "raiz" analisado
        this.direita.inserir(novoNó); // Recursividade.
      }
    } else {
      // Valor sendo inserido já existe na minha árvore.
      // Fazer nada.
    }

  }

  busca(valorBuscado) {

    if(this.valor == valorBuscado) {
      // O nó armazena o valor sendo buscado.
      return true;
    } else {
      if(this.valor > valorBuscado) {
        // Valor sendo buscado é MENOR que o valor do nó.
        // Visitar subárvore à esquerda.
        if(this.esquerda) {
          // Subárvore à esquerda existe.
          // Temos que continuar buscando, dessa vez nela.
          return this.esquerda.busca(valorBuscado); // Chamada recursiva.
        } else {
          // Subárvore à esquerda NÃO existe.
          // A busca chega ao fim e o valor buscado não foi encontrado.
          return false;
        }

      } else {
        // Valor sendo buscado é MAIOR que o valor do nó.
        // Visitar subárvore à direita.
        if(this.direita) {
          // Subárvore à direita existe.
          // Temos que continuar buscando.
          return this.direita.busca(valorBuscado); // Chamada recursiva.
        } else {
          // Subárvore à direita NÃO existe.
          // A busca chega ao fim e o valor buscado não foi encontrado.
          return false;
        }
      }
    }

  }

  percursoPréOrdem(lista) {
    // Executa a operação antes de visitar a subárvore da esquerda e da direita.

    // OPERAÇÃO: adiciona o valor do nó em uma lista
    lista.push(this.valor);

    if(this.esquerda) {
      // Visita a subárvore da esquerda (se ela existir), fazendo esse mesmo percurso nela
      this.esquerda.percursoPréOrdem(lista);
    }
    if(this.direita) {
      // Visita a subárvore da direita (se ela existir), fazendo esse mesmo percurso nela
      this.direita.percursoPréOrdem(lista);
    }

  }

  percursoEmOrdem(lista) {
    // Executa a operação após visitar a subárvore da esquerda e antes de visitar a subárvore da direita.

    if(this.esquerda) {
      // Visita a subárvore da esquerda (se ela existir), fazendo esse mesmo percurso nela
      this.esquerda.percursoEmOrdem(lista);
    }

    // OPERAÇÃO: adiciona o valor do nó em uma lista
    lista.push(this.valor);

    if(this.direita) {
      // Visita a subárvore da direita (se ela existir), fazendo esse mesmo percurso nela
      this.direita.percursoEmOrdem(lista);
    }

  }

  percursoPósOrdem(lista) {
    // Executa a operação após visitar a subárvore da esquerda e da direita.

    if(this.esquerda) {
      // Visita a subárvore da esquerda (se ela existir), fazendo esse mesmo percurso nela
      this.esquerda.percursoPósOrdem(lista);
    }
    if(this.direita) {
      // Visita a subárvore da direita (se ela existir), fazendo esse mesmo percurso nela
      this.direita.percursoPósOrdem(lista);
    }

    // OPERAÇÃO: adiciona o valor do nó em uma lista
    lista.push(this.valor);

  }

}

module.exports = Nó;